<?php # Blah 

	#require_once ("libs/sqldb.php");

?>
<!doctype html>
<html>
<head> 

    <meta charset="utf-8">
    <!-- TemplateBeginEditable name="doctitle" -->
    <title>Improve Group LLN Questionnaire</title>
    <!-- TemplateEndEditable -->
    <!-- TemplateBeginEditable name="head" -->
    <!-- TemplateEndEditable -->
   
   
   
   <script src="js/modernizr.custom.38127.js"></script>
   <script>Modernizr.load({
  test: Modernizr.inputtypes.date,
  nope:  
  [
 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js',
   'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.min.js', 
   'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css',
   'jquery-ui.css'],
   
  complete: function () {
    $('input[type=date]').datepicker({
      dateFormat: 'yy-mm-dd'
    }); 
  }
});
</script>
   
   
  
   
    <link href="llmCSS.css" rel="stylesheet" type="text/css">
    
   
    
  
 
    
    
    
    <script>
function validateForm() {
    var x = document.forms["llnentry"]["sname"].value;
    var y = document.forms["llnentry"]["dob"].value;
{
if (x == null || x == "") 
{
alert("Name must be filled out");
event.preventDefault();
return false;

}
}

{
if (y == null || y == "") 
{
alert("Date of Birth must be filled out");
event.preventDefault();
return false;

}
}

}
</script>




</head>

<body>
    <form onkeypress="return event.keyCode != 13;" action="theform.php" method="post" name="llnentry" onSubmit = "return validateForm();">
    
        <div style="max-width:880px; width:100%;  font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif; background-color:#DAF6F8; margin-left:auto; margin-right:auto; padding-top:50px">
        
            <span style="font-size:60px;">
                <div align="center">LANGUAGE, LITERACY & NUMERACY DETERMINATION</div>
            </span>
            <img src="images/img1.png" height="400px" width="900px"/>
            
            <div style = "padding-left:60px; padding-right:60px;">
            
                <div class="back1">
                    <p>The following Language, Literacy and Numeracy assessment is a mandatory requirement under the Vocational Education and Training learning system. This assessment of your language, literacy and numeracy skills is to your advantage as it assists the Registered Training Organisation and the trainer to make adjustments to the training delivery and assessment process to meet any individual requirements.
                    The information contained in this document will be considered <b>CONFIDENTIAL</b> after completion. Improve Group Learning Solutions will take all reasonable precautions to prevent this document being viewed by unauthorized persons.</p>
                 </div>
                    
                    
                <h2 align="center">Completing this Document</h2>
                <p>There is no time limit in which to complete this document. You can however submit <strong>ONLY ONE</strong> set of responses, so please be sure of your answers before clicking the submit button.  If you experience any difficulty interpreting or understanding any aspect of this document please seek assistance from your Registered Training Organisation representative.</p>
                                
                <p><b> Your Name </b>
                    <input name="sname" type="text" autofocus required="required" tabindex="1"  size="70" >
                </p>
                <p><b>Date of Birth</b> </b><input name="dob"  id="datepicker" type="date" autofocus required="required"></p>
                 <p>&nbsp;</p>
                 <p>&nbsp;</p>
               <div align="center">
                         <input type="submit" name="start"  value="Start Questionnaire" style="width:150px;height:60px">
                    </div>
                    
                    
                    
                
          
                    
                    
        	</div>
        </div>
    </form>
</body>
</html>
