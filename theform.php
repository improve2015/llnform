<?php # Blah 

	require_once ("libs/sqldb.php");

?>
<!doctype html>
<html>
<head> 

    <meta charset="utf-8">
    <!-- TemplateBeginEditable name="doctitle" -->
    <title>Improve Group LLN Questionnaire</title>
    <!-- TemplateEndEditable -->
    <!-- TemplateBeginEditable name="head" -->
    <!-- TemplateEndEditable -->
    <link href="llmCSS.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php
	# Check and format input
	# Check input against array
	# Error message and halt if found a match

	if (!isset($_POST["sname"]) OR (is_null($_POST["sname"]))) {
		echo "No Access to this file"; exit;	### OMKAR CHANGE THIS
	}
	if (!isset($_POST["dob"]) OR (is_null($_POST["dob"]))) {
		echo "No Access to this file"; exit;	### OMKAR CHANGE THIS
	}
	
	$name = str_replace(" ","",strtoupper($_POST["sname"]));
	$dob = $_POST["dob"];
	
		
	for ($i=0; $i < count($LLN_ENTRIES); $i++) 
		{ 
			$LLN_ENTRIES[$i][1].$LLN_ENTRIES[$i][2]."<br>"; //echo
			if (($LLN_ENTRIES[$i][1] == $name) AND ($LLN_ENTRIES[$i][2] == $dob)) 
			
				{
					break;
				}
		
		}
	
	if ($i != count($LLN_ENTRIES)) 
				{
					#BING BONG, already here
					echo "Sorry you can only fill this form once. <br> Our records indicate that you have already filled this form on ".$LLN_ENTRIES[$i][3];
			
					exit;
				}
	

?>
    <form onkeypress="return event.keyCode != 13;" action="phpsubmit.php" method="post">
    
        <div style="max-width:880px; width:100%;  font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif; background-color:#DAF6F8; margin-left:auto; margin-right:auto; padding-top:50px">
        
            <span style="font-size:60px;">
                <div align="center">LANGUAGE, LITERACY & NUMERACY DETERMINATION</div>
            </span>
            <img src="images/img1.png" height="400px" width="900px"/>
            
            <div style = "padding-left:60px; padding-right:60px;">
            
                <div class="back1">
                    <p>The following Language, Literacy and Numeracy assessment is a mandatory requirement under the Vocational Education and Training learning system. This assessment of your language, literacy and numeracy skills is to your advantage as it assists the Registered Training Organisation and the trainer to make adjustments to the training delivery and assessment process to meet any individual requirements.
                    The information contained in this document will be considered <b>CONFIDENTIAL</b> after completion. Improve Group Learning Solutions will take all reasonable precautions to prevent this document being viewed by unauthorized persons.</p>
                 </div>
                    
                    
                <h2 align="center">Completing this Document</h2>
                <p>There is no time limit in which to complete this document. You can however submit <strong>ONLY ONE</strong> set of responses, so please be sure of your answers before clicking the submit button.  If you experience any difficulty interpreting or understanding any aspect of this document please seek assistance from your Registered Training Organisation representative.</p>
                                
                <p><b> Your Name </b>
                    <input name="sname" type="text" autofocus required="required" size="70" value="<?php echo $name; ?>" readonly>
                </p>
                <p><b>Date of Birth</b><input name="dob" type="date" autofocus required="required"  value="<?php echo $dob; ?>" readonly></p>
                 
                <div class="visible">
                    <h3 align="center"><p>&nbsp;</p>
                        <p><b>Preamble</b></p>
                    </h3>
                    <p>Your friend, Peter, has just started an apprenticeship as a glazier and has asked you to help him with a job his boss (Jim) has asked him to complete.</p>
                    <p>The job requires Peter to measure some sheets of glass for two new windows requested by Mrs Jones at 12 Smith Street, Jonesville in New South Wales, approximately 20 minutes away from the workshop.
                    To help him out you need to complete the following activities.
                    </p>
                    <h3 align="center"><p><b>Activity 1- Language</b></p></h3>
                    
                    Q1. What is the name of the customer? 
                    <input name="cname" type="text" tabindex="1"></br></br>
                    Q2. In which town does the customer live?
                    <input name="ctown" type="text" tabindex="2"></br></br>
                    Q3. How many windows does the customer need to have installed? 
                    <input name="cwind" type="number" tabindex="3"></br></br>
                    Q4. What is the name of Peter's boss? 
                    <input name="pboss" type="text" tabindex="4"></br></br>
                    
                  <h3 align="center"><b>Activity 2 - Literacy</b></h3> 
                    
                    <p>Peter was provided with the following note on Friday and knows that there are some grammar and formatting errors – he wants you to help by double checking the grammar.</p>
                    
                    
                    <p><i>“Last thursday everthing was wrong. Cause of the rain jimmy started work 3 hour late. said his motorbike would not start, so this put us behind straight away but the real problem was that the jobb schedules had not been prepared and posted on the notiss boards for the boys and I had to find the key to the production office to get the schedules that were on kims desk”</i></p>
                    
                    <p><b>Use the section below to select the appropriate word for each error identified </b></p>
                    
                    <p>
                        <i>“Last 
                        
                        <Select name="l1" tabindex="5">
                            <option> Select</option>
                            <option value= "Thursday">Thursday</option>
                            <option value= "thursday">thursday</option>
                            <option value= "day"> day</option>
                            <option value= "days"> days</option>
                        </select>
                        
                        <Select name="l2" tabindex="6">
                          <option > Select</option>
                            <option value= "everything"> everything</option>
                            <option value= "somethings">somethings</option>
                            <option value= "everythings"> everythings</option>
                            <option value= "somethings"> somethings</option>
                        </select>
                        was wrong. 
                        
                        <Select name="l3" tabindex="7">
                            <option > Select</option>
                            <option value= "because"> because</option>
                            <option value= "Because">Because</option>
                            <option value= "hence"> hence</option>
                            <option value= "thus"> thus</option>
                        </select>
                        
                        of the rain 
                        
                        <Select name="l4" tabindex="8">
                            <option >Select</option>
                            <option value= "Jimmy"> Jimmy</option>
                            <option value= "jimmys">jimmys</option>
                            <option value= "jimmy"> jimmy</option>
                        </select>
                        
                        
                        started work 3 
                        
                        <Select name="l5" tabindex="9">
                            <option > Select</option>
                            <option value= "hour"> hour</option>
                            <option value= "hours">hours</option>
                            <option value= "huor"> huor</option>
                            <option value= "huors"> huors</option>
                        </select>
                        
                        late. 
                        <Select name="l6" tabindex="10">
                            <option > Select</option>
                            <option value= "He said"> He said</option>
                            <option value= "Said">Said</option>
                            <option value= "she say"> she say</option>
                            <option value= "he says"> he says</option>
                        </select>
                        
                        
                        
                        his motorbike would not start, so this put us behind straight away. 
                        
                        
                        <Select name="l7" tabindex="11">
                            <option > Select</option>
                            <option value= "but"> but</option>
                            <option value= "But">But</option>
                            <option value= "bat"> Bat</option>
                            <option value= "that"> that</option>
                        </select>
                        
                        
                        
                        
                        the real problem was that the 
                        <Select name="l8" tabindex="12">
                            <option > Select</option>
                            <option value= "Job"> Job</option>
                            <option value= "job">job</option>
                            <option value= "jobb"> jobb</option>
                            <option value= "jobs"> jobs</option>
                        </select> 
                        
                        schedules had not been prepared and posted on the 
                        <Select name="l9" tabindex="13">
                            <option > Select</option>
                            <option value= "notice"> notice</option>
                            <option value= "notise">notise</option>
                            <option value= "notiss"> notiss</option>
                            <option value= "notices"> notices</option>
                        </select>
                        boards for the boys. I had to find the key to the production office to get the schedules that were on 
                        
                        <Select name="l10" tabindex="14">
                            <option > Select</option>
                            <option value= "Kim"> Kim</option>
                            <option value= "Kim's">Kim's</option>
                            <option value= "kim"> kim</option>
                            <option value= "kim's"> kim's</option>
                        </select> 
                        
                        desk”
                      </i>
                    </p>
                    
                    
                    <h3 align="center"><p><b>Activity 3 - Numeracy</b></p></h3>
                    
                    <div align="center">
                     <img src="images/image.png" width="500" height="250" >
                    </div>
                    
                    Peter has been instructed by his boss to calculate the area of each glass sheet by multiplying the height by the width of each of the sheets of glass (H x W). For each of the sheets shown above calculate the area of the glass in square metres (m²).
                    
                    <p>Q1. Panel A area=<input name="paw" type="number" tabindex="15"> mm(W) x <input name="pah" type="number" tabindex="16"> mm(H) =<input name="pasqm" type="text" tabindex="17" size="4">m².</p>
                    <p>Q2. Panel B area=<input name="pbw" type="number" tabindex="18"> mm(W) x <input name="pbh" type="number" tabindex="19"> mm(H) =<input name="pbsqm" type="text" tabindex="20" size="4">m².</p>
                    
                    Jim has just told Peter that he now needs to calculate the weight of each of the glass sheets to make sure that he can lift them safely. He has told Peter that you can calculate the weight of the glass by taking the dimensions of the panels (as used above) and multiplying this by the glass thickness (6mm) and also multiplying by a factor of 2.5. <p>For each of the sheets shown above calculate the area of the glass.</p>
                    <p>Q3. Panel A weight =<input name="pawt" type="text" tabindex="21">(m²) x 6 x 2.5 = <input name="pakg" type="text" tabindex="22">Kg</p>
                    <p>Q4. Panel B weight =<input name="pbwt" type="text" tabindex="23">(m²) x 6 x 2.5 = <input name="pbkg" type="text" tabindex="24">Kg</p>
                    
                    Peter leaves the workshop at 7:00am in the morning and it takes 20 minutes to travel from the workshop to Mrs Jones’s premises, and another 2 hours to complete the window installation. He also has to travel back to the workshop after the job has been completed.
                    
                    <p>Q5. How long will Peter be away from the workshop?</br> <input name="hrs" type="number" tabindex="25" > hours <input name="min" type="number" tabindex="26" > minutes</p>
                    
                    <p>Q6. What time would you expect Peter to be back and available for the next job? <input name="time" type="text" tabindex="27" size="10">am</p>
                    
                    <div align="center">
                         <input type="submit" name="submit"  value="Submit Answers" style="width:150px;height:60px">&nbsp;&nbsp;&nbsp; <input type="reset" value="Clear Answers" style="width:150px;height:60px"> 
                    </div>
                    
                </div> 
        	</div>
        </div>
    </form>
</body>
</html>
