<?php 

date_default_timezone_set("Australia/Brisbane");    ## Hardcode Timezone

	$GLOBALS['SERVER_LOC']="1"; # DB Server Location. 0= Local WAMP, 1=Azure

	
	// Local Database Credentials for Appropriate Server
	switch ($SERVER_LOC) {
		case 0: // Local WAMP
				$DBServer 	= 'localhost';
				$DBUser   	= 'root';
				$DBPass   	= '';
				$GLOBALS['DBName'] 	= 'llnform';
				$GLOBALS['SRV_LOC']="Local WAMP";	
				break;
		case 1: //Azure
				$DBServer 	= 'au-cdbr-azure-east-a.cloudapp.net';
				$DBUser   	= 'bcc72082497bb5';
				$DBPass   	= '887a90ae';
				$GLOBALS['DBName'] 	= 'RTODashAG1xI6gyZ';
				$GLOBALS['SRV_LOC']="Azure";	
				break;
	}
	
	
	$DBConn		= new mysqli($DBServer, $DBUser, $DBPass, $DBName, 3306);
	
	if ($DBConn->connect_error) {
	  trigger_error('Database connection failed: '  . $DBConn->connect_error, E_USER_ERROR);
	}

	######################################################################################################
	$GLOBALS['LLN_ENTRIES'] = array();		# LLN Entries
	
    ########################
    ## Grab LLN Entries
    ########################
	
    # Read LLN data
	$cSQL = "SELECT  * FROM `".$DBName."`.`llnentries`;";
	
	$cSQLResult = $DBConn->query($cSQL) or die($cSQL . " " . $DBConn->error);
	
	if ($cSQLResult->num_rows > 0) {
    	while($crow = $cSQLResult->fetch_assoc()) {
        	 array_push($LLN_ENTRIES,array($crow["stu_id"],str_replace(" ","",strtoupper($crow["stu_name"])),$crow["stu_dob"],$crow["stu_timestamp"]));
    	}
	} else {
		die("LLN Entry table is empty.");
	}



?>